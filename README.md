# README #

* This is the code for  __Cubic Explorer__ along with __MaruGen__, a system to generate worlds and mechanics using a ruleset system based on the __Machinations Framework__ and the __Mission-Space Framework__ by __Joris Dormans__.

* The code is licensed under [__BSD-3-Clause License__](https://opensource.org/licenses/BSD-3-Clause).
* The artwork is licensed under a [__Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License__](href="http://creativecommons.org/licenses/by-nc-nd/4.0/). ![Creative Commons License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)

### What is MarguGen? ###

* MaruGen is a design tool for Unity. It is based on the Micro-Machinations and Mission/Space frameworks (by Joris Dormans).
* Currently this is version 0.1.0

### Who do I talk to? ###

* You can contact me on Twitter, user name @ glitchypixel
* You can also watch our website www.glitchypixel.com.co and send us messages or an email